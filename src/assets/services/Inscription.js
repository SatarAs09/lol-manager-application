import axios from "axios";
const API_URL = "http://localhost:8080";

export default class Inscription {
    static addUser = (user) =>
        axios
            .post(`${API_URL}/api/sign-up`, {
                firstname: user.firstname,
                lastname: user.lastname,
                email: user.email,
                password: user.password,
                pseudo: user.pseudo,
                age: user.age
            })
            .then(function(response) {
                console.log(response);
            })
            .catch(function(error) {
                console.log(error);
            });
}
