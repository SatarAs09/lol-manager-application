import axios from "axios";
const API_URL = "http://localhost:8080";

export default class GameService {
  static addGame = (game) =>
    axios
      .post(`${API_URL}/api/create-partie`, {
        status: 1,
        created_at: game.created_at,
        created_by: game.created_by,
        name: game.name,
        nb_joueurs: game.nb_joueurs,
      })
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error);
      });
}
