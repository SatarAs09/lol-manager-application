import axios from "axios";
const API_URL = "http://localhost:8080";

export default class Connexion {
    static ConnectUser = (connection) =>
        axios
            .post(`${API_URL}/api/login`, {
                pseudo: connection.pseudo,
                password: connection.password,
            })
            .then(function(response) {
                console.log(response);
            })
            .catch(function(error) {
                console.log(error);
            });
}
