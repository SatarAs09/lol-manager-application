import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";

import "assets/scss/material-kit-react.scss?v=1.8.0";
// pages for this product
import  Components  from "views/Components/Components.js";
import LandingPage from "views/LandingPage/LandingPage.js";
import ProfilePage from "views/ProfilePage/ProfilePage.js";
import LoginPage from "views/LoginPage/LoginPage.js";
import CreateParty from "./views/CreatePartie/CreatePartie";
import AdminPage from "./views/AdminUser/AdminUser";
import AdminPartie from "./views/AdminPartie/AdminParti";
import AdminTournois from "./views/AdminTournois/AdminTournois";
import ProfilOption from "./views/ProfilOption/ProfilOption";
import MentionsLegales from "./views/MentionsLegales/MentionsLegales";
import AdminHome from "./views/AdminHome/AdminHome";
import PartieCreee from "./views/CreatePartie/PartieCreee";
var hist = createBrowserHistory();

ReactDOM.render(
    <Router history={hist}>
        <Switch>
            <Route path="/landing-page" component={LandingPage} />
            <Route path="/profile-page" component={ProfilePage} />
            <Route path="/login-page" component={LoginPage} />
            <Route path="/create-partie" component={CreateParty} />
            <Route path="/admin-page" component={AdminPage} />
            <Route path="/admin-partie-page" component={AdminPartie} />
            <Route path="/admin-tournois" component={AdminTournois} />
            <Route path="/profil-option" component={ProfilOption}/>
            <Route path="/mentions-legales" component={MentionsLegales}/>
            <Route path="/admin" component={AdminHome}/>
            <Route path="/partie-creee" component={PartieCreee} />
            <Route path="/" component={Components} />
        </Switch>
    </Router>,
    document.getElementById("root")
);
