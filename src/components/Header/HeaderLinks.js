/*eslint-disable*/
import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
// react components for routing our app without refresh
import {Link} from "react-router-dom";

// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/material-kit-react/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
    const classes = useStyles();
    return (
        <List className={classes.list}>
            <ListItem className={classes.listItem}>
                <Link to ='/' style={{textDecoration: "none"}}>
                    <Button  color="transparent" style={{color:'gold'}} variant="contained"> <strong style={{fontSize:'16px'}}>Accueil</strong></Button>
                </Link>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Link to ='/profil-option' style={{textDecoration: "none"}}>
                <Button  color="transparent" style={{color:'gold'}} variant="contained"> <strong style={{fontSize:'16px'}}>Profil</strong></Button>
                </Link>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Link to ='/create-partie' style={{textDecoration: "none"}}>
                <Button color="transparent"  style={{color:'gold'}} variant="contained"><strong style={{fontSize:'16px'}}>Créer une partie</strong></Button>
                </Link>
            </ListItem>
        </List>
    );
}
