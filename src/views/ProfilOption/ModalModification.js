import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',

    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid gold',
        padding: theme.spacing(2, 4, 3),

    },
}));

export default function TransitionsModal() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <Button style={{marginBottom: '8px'}} variant="contained" style={{
                backgroundColor: 'gold',
                color: 'black',
                webkitBoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)',
                MozBoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)',
                BoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)'
            }} onClick={handleOpen}>
                Modifier Mon profil
            </Button>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper} align="center">
                        <h3 id="transition-modal-title">Modification Des informations Personnelles</h3>
                        <GridContainer  justify="center">
                            <GridItem xs={12} sm={12} md={6} align="center">
                            <TextField id="standard-basic" label="Prénom" value="Clément"/>
                        </GridItem>
                            <GridItem xs={12} sm={12} md={6} align="center">
                                <TextField id="standard-basic" label="Nom" value="ALALA"/>
                            </GridItem>
                        </GridContainer>
                        <GridContainer style={{marginBottom:'4%'}} justify="center">
                            <GridItem xs={12} sm={12} md={6} align="center">
                                <TextField id="standard-basic" type="number" min="15" value="34" label="Age"/>
                            </GridItem>
                            <GridItem xs={12} sm={12} md={6} align="center">
                                <TextField id="standard-basic" value="Yuri" label="Profil RIOT lié"/>
                            </GridItem>
                        </GridContainer>


                        <Button style={{marginRight: '2px'}} variant="contained" color="success">
                            Valider
                        </Button>
                        <Button style={{marginLeft: '2px'}} color="secondary" onClick={handleClose} han variant="contained">
                           Retour
                        </Button>
                    </div>

                </Fade>
            </Modal>
        </div>
    );
}
