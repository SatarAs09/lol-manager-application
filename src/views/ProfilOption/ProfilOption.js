import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
import {makeStyles} from "@material-ui/core/styles";
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import profile from "assets/img/examples/maxresdefault.jpg";
import styles from "assets/jss/material-kit-react/views/profilePage.js";
import ModalModification from "./ModalModification";
import ModalSupp from "./ModalSupp";

const useStyles = makeStyles(styles);

export default function ProfilePage(props) {
    const classes = useStyles();
    const {...rest} = props;
    const imageClasses = classNames(
        classes.imgRaised,
        classes.imgRoundedCircle,
        classes.imgFluid
    );
    const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);

    return (
        <div>
            <Header
                color="transparent"
                brand="Material Kit React"
                rightLinks={<HeaderLinks/>}
                fixed
                changeColorOnScroll={{
                    height: 200,
                    color: "white"
                }}
                {...rest}
            />
            <Parallax small filter image={require("assets/img/wallhaven-k9eqdm.jpg")}/>
            <div className={classNames(classes.main, classes.mainRaised)}>

                <div className={classes.container}>
                    <GridContainer justify="center">

                        <GridItem xs={12} sm={12} md={6} align="center">
                            <div className={classes.profile} align="center">
                                <div >
                                    <img style={{height: '120px'}} src={profile} alt="..."/>
                                    <h2 style={{marginTop: '-5%'}}><strong>""YURI""</strong></h2>

                                </div>
                            </div>

                        </GridItem>

                    </GridContainer>

                    <GridContainer style={{
                        backgroundColor: "#fbfbfb",
                        borderTopLeftRadius: '10px',
                        borderTopRightRadius: '10px',
                        padding: '10px'
                    }}>
                        <GridItem xs={12} sm={6} md={4} className={classes.navWrapper}>
                            <div align="left">
                                <p style={{fontSize: '20px'}}>Nom : <strong>ALALA</strong></p>
                                <p style={{fontSize: '20px'}}>Prénom : <strong> Clément</strong></p>
                                <p style={{fontSize: '20px'}}> Age : <strong> 34 ans </strong></p>
                                <p style={{fontSize: '16px'}}>Date d'inscription : 10/10/2019 </p>
                                <br/>
                                <ModalModification />
                            </div>
                        </GridItem>
                        <GridItem xs={12} sm={6} md={8} className={classes.navWrapper} justify="center">
                            <p style={{fontSize: '20px'}}>Compte RiotGames Lié : <strong>Yuri</strong></p>

                        </GridItem>
                    </GridContainer>

                    <div align="center" style={{marginTop:'10px'}}>
                        <ModalSupp />

                    </div>


                </div>
            </div>
            <Footer/>
        </div>
    );
}
