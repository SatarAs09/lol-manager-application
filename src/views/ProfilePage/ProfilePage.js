import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";
import Paper from "@material-ui/core/Paper";
import profile from "assets/img/examples/maxresdefault.jpg";
import styles from "assets/jss/material-kit-react/views/profilePage.js";
import Rank from '../../assets/img/challenger_1.png';
import Gold from '../../assets/img/gold.png';
import SectionTabs from "../Components/Sections/SectionTabs";
import Button from '@material-ui/core/IconButton';
import SettingsIcon from '@material-ui/icons/Settings';
import Tooltip from "@material-ui/core/Tooltip";
import {Link} from "react-router-dom";


const useStyles = makeStyles(styles);

export default function ProfilePage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );
  const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);
  return (
    <div>
      <Header
        color="transparent"
        brand="Material Kit React"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 200,
          color: "white"
        }}
        {...rest}
      />
      <Parallax small filter image={require("assets/img/wallhaven-3kovev.jpg")} />
      <div className={classNames(classes.main, classes.mainRaised)}>


          <div className={classes.container}>

            <GridContainer justify="center">

              <GridItem xs={12} sm={12} md={6}>

                <div className={classes.profile}>
                  <div>
                    <img style={{height:'120px'}} src={profile} alt="..."  />
                      <h2 style={{marginTop:'-5%'}}><strong>""YURI""</strong></h2>

                    </div>
                </div>
              </GridItem>

            </GridContainer>

            <GridContainer style={{backgroundColor: "#fbfbfb",
                borderTopLeftRadius: '10px',
                borderTopRightRadius: '10px',
                padding: '10px'}} >
              <GridItem xs={12} sm={6} md={2} className={classes.navWrapper} >

                  <Paper justify="left">
                      <img justify="left" style={{width:'20%'}} src={Rank}/>
                      <p>Ranked Solo</p>
                      <h5>Unranked</h5>
                  </Paper>
                  <Paper justify="left">
                      <img justify="left" style={{width:'20%'}} src={Gold}/>
                      <p>Flex 5:5 Rank</p>
                      <h5>Gold</h5>
                  </Paper>

              </GridItem>
                <GridItem xs={12} sm={6} md={10} className={classes.navWrapper} justify="center">

                        <SectionTabs />

                </GridItem>
            </GridContainer>

          </div>
      </div>
      <Footer />
    </div>
  );
}
