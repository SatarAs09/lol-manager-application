import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import image from "assets/img/wallhaven-017vm1.jpg";
import SectionAdminUserTab from "../Components/Sections/SectionAdminUserTab";
import {Link} from "react-router-dom";
import PeopleIcon from "@material-ui/icons/People";
import GamesIcon from '@material-ui/icons/Games';
import GradientIcon from '@material-ui/icons/Gradient';
import HomeIcon from '@material-ui/icons/Home';
import profile from "assets/img/logoprojet.png";
import KeyboardReturnIcon from "@material-ui/icons/KeyboardReturn";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',

    },
    drawer: {
        [theme.breakpoints.up('md')]: {
            width: drawerWidth,
            flexShrink: 0,

        },
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
            background: 'linear-gradient(-249.36882712449835deg, rgba(0, 0, 0,1) 2.392578125%,rgba(1, 96, 186,1) 91.58528645833333%)',
            color: 'white'
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
        backgroundImage: "url(" + image + ")",
        color: 'white'

    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),


    },
    table: {
        minWidth: 650,
    },
}));

function ResponsiveDrawer(props) {
    const {container} = props;
    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <div>
            <Link to ='/admin' style={{textDecoration: "none"}}>
            <img style={{width:'120px', marginLeft:'25%', marginTop: '3%'}} src={profile} alt="..."  />
            </Link>
            <div className={classes.toolbar}/>

            <List>
                <Link to ='/admin' style={{textDecoration: "none"}}>
                    <ListItem button>
                        <ListItemIcon style={{color:"gold", fill:'gold'}}>
                            <HomeIcon className={classes.icon} />
                        </ListItemIcon>
                        <ListItemText primary="Accueil" />
                    </ListItem>
                </Link>
                <Link to ='/admin-page' style={{textDecoration: "none"}}>
                    <ListItem button>
                        <ListItemIcon style={{color:"gold", fill:'gold'}}>
                            <PeopleIcon className={classes.icon} />
                        </ListItemIcon>
                        <ListItemText primary="Utilisateurs" />
                    </ListItem>
                </Link>
                <Link to ='/admin-partie-page' style={{textDecoration: "none"}}>
                    <ListItem button>
                        <ListItemIcon style={{color:"gold", fill:'gold'}}>
                            <GamesIcon className={classes.icon} />
                        </ListItemIcon>
                        <ListItemText primary="Parties" />
                    </ListItem>
                </Link>
                <Link to ='/admin-tournois' style={{textDecoration: "none"}}>
                    <ListItem button>
                        <ListItemIcon style={{color:"gold", fill:'gold'}}>
                            <GradientIcon className={classes.icon} />
                        </ListItemIcon>
                        <ListItemText primary="Tournois" />
                    </ListItem>
                </Link>
                <hr style={{width: '90px'}}/>
                <Link to ='/' style={{textDecoration: "none"}}>
                    <ListItem button>
                        <ListItemIcon style={{color:"gold", fill:'gold'}}>
                            <KeyboardReturnIcon className={classes.icon} />
                        </ListItemIcon>
                        <ListItemText primary="Retour sur le site" />
                    </ListItem>
                </Link>

            </List>


        </div>
    );

    return (
        <div className={classes.root}>
            <CssBaseline/>
            <AppBar position="fixed" justify="center" className={classes.appBar}>
                <Toolbar justify="center">
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Typography align="center" variant="h5" noWrap>
                        Derniers utilisateurs inscrit
                    </Typography>
                </Toolbar>
            </AppBar>
            <nav className={classes.drawer} aria-label="mailbox folders">
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Hidden smUp implementation="css">
                    <Drawer
                        container={container}
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,

                        }}
                        variant="permanent"
                        open
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
            <main className={classes.content} style={{backgroundColor: '#h2h2h2'}}>
                <div className={classes.toolbar}/>
                <SectionAdminUserTab />
            </main>
        </div>
    );
}

ResponsiveDrawer.propTypes = {
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    container: PropTypes.any,
};

export default ResponsiveDrawer;
