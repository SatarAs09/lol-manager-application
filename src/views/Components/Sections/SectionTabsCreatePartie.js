import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
// import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "components/CustomButtons/Button.js";

import GameService from "../../../assets/services/GameServices";
import {Link} from "react-router-dom";

const currencies = [
  {
    value: "2",
    label: "2 personnes",
  },
  {
    value: "3",
    label: "3 personnes",
  },
  {
    value: "4",
    label: "4 personnes",
  },
  {
    value: "5",
    label: "5 personnes",
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
}));

const SectionTabsCreatePartie = () => {
  const classes = useStyles();
  const [currency, setCurrency] = React.useState("EUR");
  const [game, setGame] = useState(null);

  const addGame = () => {
    GameService.addGame(game)
      .then(() => {
        console.log(Response);
      })
      .catch((err) => {
        alert("Impossible de créer le partie");
      });
  };

  const changeName = (name) => {
    setGame({
      ...game,
      name,
    });
  };

  const changeNb_joueurs = (nb_joueurs) => {
    setGame({
      ...game,
      nb_joueurs,
    });
  };

  const changeDate = (created_at) => {
    setGame({
      ...game,
      created_at,
    });
  };
  const handleChange = (event) => {
    setCurrency(event.target.value);
  };

  return (
    <div className={classes.section}>
      <div className={classes.container}>
        <div id="nav-tabs">
          <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={12} justify="center">
              <CustomTabs
                plainTabs
                headerColor="success"
                tabs={[
                  {
                    tabName: "Partie",
                    tabContent: (
                      <div style={{ textAlign: "center" }}>
                        <h2 style={{ marginBottom: "5%" }}>
                          Création d'une Partie
                        </h2>
                        <GridContainer
                          style={{ marginTop: "2%" }}
                          justify="center"
                        >
                          <GridItem xs={6} sm={6} md={6}>
                            {/* <FormControl
                              fullWidth
                              className={classes.margin}
                              variant="outlined"
                            >
                              <InputLabel
                                htmlFor="outlined-adornment-amount"

                                name="name"
                                id="name"
                                onChange={(e) => changeName(e.target.value)}
                              >
                                Nom de la partie
                              </InputLabel>
                              <OutlinedInput
                                id="outlined-adornment-amount"
                                labelWidth={60}
                              />
                            </FormControl> */}
                            <TextField
                              variant="filled"
                              id="name"
                              name="name"
                              label="Nom de la partie"
                              onChange={(e) => changeName(e.target.value)}
                            >
                              Nom de la partie
                            </TextField>
                          </GridItem>
                          <GridItem xs={6} sm={6} md={6}>
                            <TextField
                              id="nb_joueurs"
                              name="nb_joueurs"
                              select
                              label="Nombre de joueurs"
                              value={currency}
                              helperText="Choisissez un nombre"
                              variant="filled"
                              labelWidth={60}
                              onChange={(e) => changeNb_joueurs(e.target.value)}
                            >
                              {currencies.map((option) => (
                                <MenuItem
                                  key={option.value}
                                  value={option.value}
                                >
                                  {option.label}
                                </MenuItem>
                              ))}
                            </TextField>
                          </GridItem>
                        </GridContainer>
                        <hr style={{ marginTop: "2%" }} />

                        <h3>Pseudo des joueurs à inviter</h3>

                        <GridContainer
                          style={{ marginTop: "2%" }}
                          justify="center"
                        >
                          <GridItem xs={4} sm={4} md={4}>
                            <FormControl
                              fullWidth
                              className={classes.margin}
                              variant="outlined"
                            >
                              <InputLabel htmlFor="outlined-adornment-amount">
                                Pseudo
                              </InputLabel>
                              <OutlinedInput
                                id="outlined-adornment-amount"
                                labelWidth={60}
                              />
                            </FormControl>
                          </GridItem>
                          <GridItem xs={4} sm={4} md={4}>
                            <FormControl
                              fullWidth
                              className={classes.margin}
                              variant="outlined"
                            >
                              <InputLabel htmlFor="outlined-adornment-amount">
                                Pseudo
                              </InputLabel>
                              <OutlinedInput
                                id="outlined-adornment-amount"
                                labelWidth={60}
                              />
                            </FormControl>
                          </GridItem>
                          <GridItem xs={4} sm={4} md={4}>
                            <FormControl
                              fullWidth
                              className={classes.margin}
                              variant="outlined"
                            >
                              <InputLabel htmlFor="outlined-adornment-amount">
                                Pseudo
                              </InputLabel>
                              <OutlinedInput
                                id="outlined-adornment-amount"
                                labelWidth={60}
                              />
                            </FormControl>
                          </GridItem>
                        </GridContainer>
                        <GridContainer
                          style={{ marginTop: "2%" }}
                          justify="center"
                        >
                          <GridItem xs={4} sm={4} md={4}>
                            <FormControl
                              fullWidth
                              className={classes.margin}
                              variant="outlined"
                            >
                              <InputLabel htmlFor="outlined-adornment-amount">
                                Pseudo
                              </InputLabel>
                              <OutlinedInput
                                id="outlined-adornment-amount"
                                labelWidth={60}
                              />
                            </FormControl>
                          </GridItem>
                          <GridItem xs={4} sm={4} md={4}>
                            <FormControl
                              fullWidth
                              className={classes.margin}
                              variant="outlined"
                            >
                              <InputLabel htmlFor="outlined-adornment-amount">
                                Pseudo
                              </InputLabel>
                              <OutlinedInput
                                id="outlined-adornment-amount"
                                labelWidth={60}
                              />
                            </FormControl>
                          </GridItem>
                          <GridItem xs={4} sm={4} md={4}>
                            <TextField
                              id="created_at"
                              name="created_at"
                              label="Date de la partie"
                              type="date"
                              defaultValue="2020-05-24"
                              onChange={(e) => changeDate(e.target.value)}
                              className={classes.textField}
                              //   InputLabelProps={{
                              //     shrink: true,
                              //   }}
                            />
                          </GridItem>
                        </GridContainer>
                      </div>
                    ),
                  },
                ]}
              />
            </GridItem>
          </GridContainer>
          <div style={{ textAlign: "center", marginTop: "-5%" }}>
            <Link to ="/partie-creee"  style={{textDecoration: "none"}}>
            <Button
              type="submit"
              onClick={addGame}
              style={{
                backgroundColor: "gold",
                padding: "20px",
                color: "black",
                fontSize: "14px",
                webkitBoxShadow: "0px 0px 12px 1px rgba(255,221,31,1)",
                MozBoxShadow: "0px 0px 12px 1px rgba(255,221,31,1)",
                BoxShadow: "0px 0px 12px 1px rgba(255,221,31,1)",
              }}
              size="lg"
            >
              Valider
            </Button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};
export default SectionTabsCreatePartie;
