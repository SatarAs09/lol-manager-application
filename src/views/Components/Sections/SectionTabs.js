import React from "react";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
import SectionTableaux from "./SectionTableaux";
import styles from "assets/jss/material-kit-react/views/componentsSections/tabsStyle.js";
import Avat from '../../../assets/img/faces/avatar.jpg';
import Avatar from "@material-ui/core/Avatar";
import {dangerColor, primaryColor} from "../../../assets/jss/material-kit-react";
import SectionTableauxChampions from "./SectionTableauxChampions";
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function SimpleTable() {
    const classes = useStyles();

    return (
        <div className={classes.section}>
            <div className={classes.container}>
                <div id="nav-tabs">
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={12}>

                            <CustomTabs
                                plainTabs
                                headerColor="success"
                                tabs={[
                                    {
                                        tabName: "Total",
                                        tabContent: (
                                            <GridContainer style={{marginTop: '2%'}}>
                                                <GridItem xs={12} sm={12} md={4}>
                                                    <GridContainer>
                                                        <GridItem xs={12} sm={12} md={6}>
                                                            <p>4.2/4.0/11.4/</p>
                                                            <h2>3.90:1 (43%)</h2>
                                                        </GridItem>
                                                        <GridItem xs={12} sm={12} md={6}>

                                                        </GridItem>
                                                    </GridContainer>
                                                </GridItem>
                                                <GridItem xs={12} sm={12} md={4}>
                                                    <GridContainer>
                                                        <GridItem xs={12} sm={12} md={2}>
                                                            <Avatar src={Avat}></Avatar>
                                                        </GridItem>
                                                        <GridItem xs={12} sm={12} md={4}>
                                                            <p>Pseudo</p>
                                                            <h6 color={dangerColor}>100%</h6>
                                                        </GridItem>
                                                        <GridItem xs={12} sm={12} md={6}>
                                                            <p color="primaryColor">Perfect KDA</p>
                                                        </GridItem>
                                                    </GridContainer>
                                                    <GridContainer>
                                                        <GridItem xs={12} sm={12} md={2}>
                                                            <Avatar src={Avat}></Avatar>
                                                        </GridItem>
                                                        <GridItem xs={12} sm={12} md={4}>
                                                            <p>Pseudo</p>
                                                            <h6 color={dangerColor}>100%</h6>
                                                        </GridItem>
                                                        <GridItem xs={12} sm={12} md={6}>
                                                            <p color="primaryColor">Perfect KDA</p>
                                                        </GridItem>
                                                    </GridContainer>

                                                </GridItem>
                                                <GridItem xs={12} sm={12} md={4}>
                                                    <p>Preffered Position (Rank)</p>
                                                    <h6>Not found Position</h6>
                                                </GridItem>
                                            </GridContainer>
                                        )
                                    },
                                    {
                                        tabName: "Parties Récentes",
                                        tabContent: (
                                            <GridContainer style={{marginTop: '2%'}}>
                                                <SectionTableaux/>
                                            </GridContainer>
                                        )
                                    },
                                    {
                                        tabName: "Champions",
                                        tabContent: (
                                            <GridContainer style={{marginTop: '2%'}}>
                                                <SectionTableauxChampions/>
                                            </GridContainer>
                                        )
                                    }
                                ]}
                            />
                        </GridItem>
                    </GridContainer>
                </div>
            </div>
        </div>
    );
}
