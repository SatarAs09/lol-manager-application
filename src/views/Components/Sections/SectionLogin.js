import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {Link} from "react-router-dom";
import Inscription from "../../../assets/services/Inscription";
import Button from "@material-ui/core/Button";
//const classes = useStyles();
const useStyles = makeStyles((theme) => ({
    root: {
        "& .MuiTextField-root": {
            margin: theme.spacing(1),
            width: "25ch",
        },
    },
}));

const LoginPage = () => {
    //const useStyles = makeStyles(styles);
    const classes = useStyles;
    const [user, setUser] = React.useState(null);
    const addUser = () => {
        Inscription.addUser(user)
            .then(() => {
                console.log(Response);
            })
            .catch((err) => {
                alert("Impossible de créer l'utilisateur");
            });
    };

    const firstname = (firstname) => {
        setUser({
            ...user,
            firstname,
        });
    };

    const lastname = (lastname) => {
        setUser({
            ...user,
            lastname,
        });
    };
    const email = (email) => {
        setUser({
            ...user,
            email,
        });
    };
    const password = (password) => {
        setUser({
            ...user,
            password,
        });
    };
    const pseudo = (pseudo) => {
        setUser({
            ...user,
            pseudo,
        });
    };
    const age = (age) => {
        setUser({
            ...user,
            age,
        });
    };

    return (
        <div className={classes.section}>
            <div className={classes.container}>
                <GridContainer justify="center">
                    <GridItem xs={12} sm={12} md={12} style={{marginTop: "2%"}}>
                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        autoComplete="fname"
                                        name="firstname"
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="firstname"
                                        label="Prenom"
                                        onChange={(e) => firstname(e.target.value)}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="lastname"
                                        label="Nom"
                                        name="lastname"
                                        onChange={(e) => lastname(e.target.value)}

                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="email"
                                        label="Email"
                                        name="email"
                                        autoComplete="email"
                                        onChange={(e) => email(e.target.value)}

                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        name="password"
                                        label="Mot de passe"
                                        type="password"
                                        id="password"
                                        autoComplete="current-password"
                                        onChange={(e) => password(e.target.value)}

                                    />
                                </Grid>

                                <Grid item xs={12}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        id="pseudo"
                                        label="Ton pseudo"
                                        name="pseudo"
                                        onChange={(e) => pseudo(e.target.value)}
                                    />
                                </Grid>

                                <Grid item md={12}>
                                    <TextField
                                        id="age"
                                        name="age"
                                        label="Age"
                                        type="date"
                                        defaultValue="2020-05-24"
                                        onChange={(e) => age(e.target.value)}
                                        className={classes.textField}
                                        //   InputLabelProps={{
                                        //     shrink: true,
                                        //   }}
                                    />
                                </Grid>
                            </Grid>

                            <Grid item md={12} alignContent="center">
                                <FormControlLabel control={<Checkbox name="checkedC"/>}
                                                  label="J'accepte les conditions d'utilisations"/>
                                <Link to='/mentions-legales'>
                                    <p style={{color: 'darkblue', textAlign: 'center'}}>Accéder aux Conditions
                                        d'utilisations</p>
                                </Link>
                            </Grid>
                            <div style={{textAlign:'center', marginTop:"3%"}}>
                                <Link to ='/profil-option' style={{textDecoration: "none"}}>
                                <Button
                                    onClick={addUser}
                                    style={{
                                    backgroundColor: 'gold',
                                    padding:'20px',
                                    color:'black',
                                    fontSize: '14px',
                                    webkitBoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)',
                                    MozBoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)',
                                    BoxShadow: '0px 0px 12px  1px rgba(255,221,31,1)',
                                }} size="lg">
                                    Valider
                                </Button>
                                </Link>
                            </div>

                    </GridItem>
                </GridContainer>
            </div>
        </div>
    );
};
export default LoginPage;
