import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

function createData(Summoner, Played, Win, Lose, WinRatio) {
    return { Summoner, Played, Win, Lose, WinRatio };
}

const rows = [
    createData('Zank', 10, 6, 4, 70),
    createData('Daddy', 10, 6, 4, 70),
    createData('Rout', 10, 8, 4, 50),
];

export default function SimpleTable() {
    const classes = useStyles();

    return (

            <Table className={classes.table} aria-label="simple table">

                <TableHead style={{backgroundColor:"#dedede"}}>
                    <TableRow>
                        <TableCell>Summoner</TableCell>
                        <TableCell align="right">Played</TableCell>
                        <TableCell align="right">Win</TableCell>
                        <TableCell align="right">Lose</TableCell>
                        <TableCell align="right">WinRatio</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.Summoner}>
                            <TableCell component="th" scope="row">
                                {row.Summoner}
                            </TableCell>
                            <TableCell align="right">{row.Played}</TableCell>
                            <TableCell align="right">{row.Win}</TableCell>
                            <TableCell align="right">{row.Lose}</TableCell>
                            <TableCell align="right">{row.WinRatio}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
    );
}
