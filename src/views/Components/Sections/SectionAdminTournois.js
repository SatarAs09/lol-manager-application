import React from 'react';

import IconButton from '@material-ui/core/IconButton';

import {makeStyles, useTheme} from '@material-ui/core/styles';
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from '@material-ui/core/Tooltip';
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';


const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 200,
    },
    tooltip: {
        fontSize: 20,
    },
}));

function createData(ID, Nom, CreatePseudo, Date, Statut) {
    return {ID, Nom, CreatePseudo, Date, Statut};
}

const rows = [
    createData('1223', 'La partie de la mort','Zank' , '10.12.2020', 'En attente'),
    createData('1224', 'Tchequé moi ça la rafale','Zouglou' , '23.05.2020', 'En attente'),



];


export default function SimpleTable() {
    const classes = useStyles();

    return (
        <div>
            <div style={{backgroundColor:'#F3F3F3'}}>
                <h3 align="center">Tournois à venir</h3>
            </div>
            <GridContainer minWidth="xs" align="center" style={{marginTop: '2%'}} justify="center">
            <Table className={classes.table} aria-label="simple table">

                <TableHead style={{backgroundColor: "#F2F2F2"}}>
                    <TableRow>
                        <TableCell>#</TableCell>
                        <TableCell>Nom</TableCell>
                        <TableCell align="right">Créateur de la partie</TableCell>
                        <TableCell align="right">Date de création</TableCell>
                        <TableCell align="right">Statut</TableCell>
                        <TableCell align="right">Action</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.ID}>
                            <TableCell component="th" scope="row">
                                {row.ID}
                            </TableCell>
                            <TableCell><strong>{row.Nom}</strong></TableCell>
                            <TableCell align="right">{row.CreatePseudo}</TableCell>
                            <TableCell align="right">{row.Date}</TableCell>
                            <TableCell align="right">{row.Statut}</TableCell>

                            <TableCell align="right">
                                <Tooltip className={classes.tooltip} title="Voir" arrow>
                                    <IconButton  aria-label="delete">
                                        <VisibilityIcon/>
                                    </IconButton>
                                </Tooltip>
                                <Tooltip className={classes.tooltip} title="Archiver" arrow>
                                    <IconButton color="secondary" aria-label="delete">
                                        <DeleteIcon/>
                                    </IconButton>
                                </Tooltip>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            </GridContainer>


            <div style={{marginTop:'3%'}}>
            </div>
            <GridContainer minWidth="xs" align="center" style={{marginTop: '2%'}} justify="center">
                <GridItem   xs={12} sm={12} md={6} align="center">
                    <div style={{backgroundColor:'#F3F3F3'}}>
                        <h6> Tournois en cours</h6>
                    </div>
                    <GridContainer >
                        <GridItem  xs={12} sm={12} md={4}>
                            <p><strong>La rafale</strong></p>
                        </GridItem>
                        <GridItem  xs={12} sm={12} md={4}>
                            <p>5 Joueurs</p>
                        </GridItem>
                        <GridItem  xs={12} sm={12} md={4}>
                            <LinearProgress variant="query" color="primary" />

                        </GridItem>
                    </GridContainer>
                    <GridContainer >
                        <GridItem  xs={12} sm={12} md={4}>
                            <p><strong>Twenty one century</strong></p>
                        </GridItem>
                        <GridItem  xs={12} sm={12} md={4}>
                            <p> 4 Joueurs</p>
                        </GridItem>
                        <GridItem  xs={12} sm={12} md={4}>
                            <LinearProgress variant="query" color="primary" />

                        </GridItem>
                    </GridContainer>
                </GridItem>
                <GridItem align="center" xs={12} sm={12} md={6}>
                    <div style={{backgroundColor:'#F3F3F3'}}>
                        <h6> Tournois Terminées</h6>
                    </div>
                    <GridContainer >
                        <GridItem  xs={12} sm={12} md={4}>
                            <p><strong>La tché</strong></p>
                        </GridItem>
                        <GridItem  xs={12} sm={12} md={4}>
                            <h6> Durée : </h6>
                            <p>23 minutes</p>
                        </GridItem>
                        <GridItem  xs={12} sm={12} md={4}>
                            <CircularProgress variant="static"  size={24} style={{color:'green'}} value={100} />
                        </GridItem>
                    </GridContainer>
                    <GridContainer >
                        <GridItem  xs={12} sm={12} md={4}>
                            <p><strong>La jungle urbaine</strong></p>
                        </GridItem>
                        <GridItem  xs={12} sm={12} md={4}>
                            <h6> Durée : </h6>
                            <p>27 minutes</p>
                        </GridItem>
                        <GridItem  xs={12} sm={12} md={4}>
                            <CircularProgress variant="static"  size={24} style={{color:'green'}} value={100} />
                        </GridItem>
                    </GridContainer>
                </GridItem>
            </GridContainer>
        </div>


    );
}
