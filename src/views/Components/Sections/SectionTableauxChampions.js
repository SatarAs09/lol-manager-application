import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

function createData(Id, Champion, Played, Kda, Gold, Cs) {
    return { Id, Champion, Played, Kda, Gold, Cs};
}

const rows = [
    createData('1', "Yuumi", '100%', '0.0 / 0.0 / 15.0', "5.364", "0.5"),
    createData('1', "Leona", '100%', '0.0 / 0.0 / 15.0', "3.086", "0.0"),

];

export default function SimpleTable() {
    const classes = useStyles();

    return (

            <Table className={classes.table} aria-label="simple table">

                <TableHead style={{backgroundColor:"#dedede"}}>
                    <TableRow >
                        <TableCell>#</TableCell>
                        <TableCell align="right"> <strong>Champion</strong></TableCell>
                        <TableCell align="right"><strong>Joué</strong></TableCell>
                        <TableCell align="right"><strong>Kda</strong></TableCell>
                        <TableCell align="right"><strong>Ratio</strong></TableCell>
                        <TableCell align="right"><strong>Cs</strong></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.Id}>
                            <TableCell component="th" scope="row">
                                {row.Id}
                            </TableCell>
                            <TableCell align="right">{row.Champion}</TableCell>
                            <TableCell align="right">{row.Played}</TableCell>
                            <TableCell align="right">{row.Kda}</TableCell>
                            <TableCell align="right">{row.Gold}</TableCell>
                            <TableCell align="right">{row.Cs}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
    );
}
