import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import CustomTabs from "components/CustomTabs/CustomTabs.js";
// core components


import styles from "assets/jss/material-kit-react/views/componentsSections/pillsStyle.js";
import SectionLogin from "./SectionLogin";
import SectionConnexion from "./SectionConnexion";

const useStyles = makeStyles(styles);

export default function SectionPills() {
  const classes = useStyles();
  return (
      <div className={classes.section}>
          <div className={classes.container}>
              <div id="nav-tabs">
                  <GridContainer>
                      <GridItem xs={12} sm={12} md={12} style={{}}>

                          <CustomTabs
                              plainTabs
                              headerColor="success"
                              tabs={[

                  {
                      tabName: "Inscription",
                      tabContent:
                    (
                      <span>
                          <SectionLogin />

                      </span>
                    )
                  },
                  {
                      tabName: "Connexion",
                      tabContent: (
                      <span>
                      <SectionConnexion />
                      </span>
                    )
                  },

                ]}
                          />
                      </GridItem>
                  </GridContainer>
              </div>
          </div>
      </div>
  );
}




