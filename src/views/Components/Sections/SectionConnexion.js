import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import styles from "assets/jss/material-kit-react/views/componentsSections/loginStyle.js";
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";
import Connexion from "../../../assets/services/Connexion";
import TextField from "@material-ui/core/TextField";

const useStyles = makeStyles(styles);

const ConnexionPage = () => {
    //const useStyles = makeStyles(styles);
    const classes = useStyles;
    const [user, setUser] = React.useState(null);

    const ConnectUser = () => {
        Connexion.ConnectUser(user)
            .then(() => {
                console.log(Response);
            })
            .catch((err) => {
                alert("Impossible de se connecter");
            });
    };

    const pseudo = (pseudo) => {
        setUser({
            ...user,
            pseudo,
        });
    };
    const password = (password) => {
        setUser({
            ...user,
            password,
        });
    };

        return (
            <div className={classes.section}>
                <div className={classes.container}>
                    <GridContainer justify="center">
                        <GridItem xs={12} sm={12} md={10}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="pseudo"
                                label="Ton pseudo"
                                name="pseudo"
                                onChange={(e) => pseudo(e.target.value)}
                            />
                            <TextField style={{marginTop:'3%'}}
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label="Mot de passe"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                onChange={(e) => password(e.target.value)}

                            />
                            <div style={{textAlign: 'center', marginTop: "3%"}}>
                                <Link to ='/profil-option' style={{textDecoration: "none"}}>

                                    <Button
                                        onClick={ConnectUser}
                                        style={{
                                            backgroundColor: 'gold',
                                            padding: '20px',
                                            color: 'black',
                                            fontSize: '14px',
                                            webkitBoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)',
                                            MozBoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)',
                                            BoxShadow: '0px 0px 12px  1px rgba(255,221,31,1)',
                                        }} size="lg">
                                        Valider
                                    </Button>
                                </Link>
                            </div>

                        </GridItem>
                    </GridContainer>
                </div>
            </div>
        );

};

export default ConnexionPage;
