import React from 'react';

import IconButton from '@material-ui/core/IconButton';

import {makeStyles, useTheme} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import VisibilityIcon from '@material-ui/icons/Visibility';
import Tooltip from "@material-ui/core/Tooltip";


const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
}));

function createData(ID, Summoner, Played, Win, Lose, WinRatio) {
    return {ID, Summoner, Played, Win, Lose, WinRatio};
}

const rows = [
    createData('1', 'Zank', 10, 6, 4, 70),
    createData('2', 'Daddy', 10, 6, 4, 70),
    createData('3', 'Rout', 10, 8, 4, 50),
    createData('4', 'Zank', 10, 6, 4, 70),
    createData('5', 'Daddy', 10, 6, 4, 70),
    createData('6', 'Rout', 10, 8, 4, 50),
    createData('7', 'Zank', 10, 6, 4, 70),
    createData('8', 'Daddy', 10, 6, 4, 70),
    createData('9', 'Rout', 10, 8, 4, 50),
];


export default function SimpleTable() {
    const classes = useStyles();

    return (
        <Table className={classes.table} aria-label="simple table">

            <TableHead style={{backgroundColor: "#F2F2F2"}}>
                <TableRow>
                    <TableCell>#</TableCell>
                    <TableCell>Joueur</TableCell>
                    <TableCell align="right">Nombre de partie jouées</TableCell>
                    <TableCell align="right">Partie gagnées</TableCell>
                    <TableCell align="right">Parties perdu</TableCell>
                    <TableCell align="right">Ratio V/D</TableCell>
                    <TableCell align="right">Action</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {rows.map((row) => (
                    <TableRow key={row.ID}>
                        <TableCell component="th" scope="row">
                            {row.ID}
                        </TableCell>
                        <TableCell><strong>{row.Summoner}</strong></TableCell>
                        <TableCell align="right">{row.Played}</TableCell>
                        <TableCell align="right">{row.Win}</TableCell>
                        <TableCell align="right">{row.Lose}</TableCell>
                        <TableCell align="right">{row.WinRatio}</TableCell>
                        <TableCell align="right">
                            <Tooltip className={classes.tooltip} title="Voir" arrow>
                                <IconButton  aria-label="delete">
                                    <VisibilityIcon/>
                                </IconButton>
                            </Tooltip>
                            <Tooltip className={classes.tooltip} title="Archiver" arrow>
                                <IconButton color="secondary" aria-label="delete">
                                    <DeleteIcon/>
                                </IconButton>
                            </Tooltip>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>

    );
}
