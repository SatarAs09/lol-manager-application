import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react components for routing our app without refresh
import {Link} from "react-router-dom";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Parallax from "components/Parallax/Parallax.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import ProductSection from "../LandingPage/Sections/ProductSection";

import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Logo from '../../assets/img/logoprojetGrand.png';
import SearchIcon from '@material-ui/icons/Search';
import styles from "assets/jss/material-kit-react/views/components.js";


const useStyles = makeStyles(styles);

export default function Components(props) {
    const classes = useStyles();
    const {...rest} = props;
    return (
        <div>
            <Header
                brand="Material Kit React"
                rightLinks={<HeaderLinks/>}
                fixed
                color="transparent"
                changeColorOnScroll={{
                    height: 400,
                    color: "white"
                }}
                {...rest}
            />
            <Parallax image={require("assets/img/backgroundHome.jpg")}>
                <div className={classes.container}>
                    <GridContainer>
                        <GridItem>
                            <div className={classes.brand} style={{textAlign: 'center'}}>
                                <h6 className={classes.title} style={{fontSize: '15px'}}>Bienvenue</h6>
                            </div>
                            <div className={classes.brand} style={{textAlign: 'center'}}>
                                <h1 className={classes.title}>Retrouvez vos Stats</h1>
                            </div>
                            <div className={classes.brand} style={{textAlign: 'center', marginTop: '2%'}}>
                                <Link to ='/login-page' style={{textDecoration: "none"}}>
                                <Button style={{
                                    backgroundColor: 'gold',
                                    color: 'black',
                                    fontSize: '20px',
                                    marginRight: '2%',
                                    webkitBoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)',
                                    MozBoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)',
                                    BoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)'
                                }}>Inscription </Button>
                                </Link>
                                <Link to ='/login-page' style={{textDecoration: "none"}}>
                                    <Button   style={{
                                        backgroundColor: 'gold',
                                        color: 'black',
                                        fontSize: '20px',
                                        webkitBoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)',
                                        MozBoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)',
                                        BoxShadow: '0px 0px 12px 1px rgba(255,221,31,1)'
                                    }}>Connexion</Button>
                                </Link>
                            </div>
                        </GridItem>
                    </GridContainer>
                </div>
            </Parallax>

            <div className={classNames(classes.main, classes.mainRaised)}
                 style={{backgroundColor: '#F1F1F1', padding: '10px'}}>

                <GridItem md={12} className={classes.textCenter} style={{padding: '2%'}}>
                    <InputBase
                        className={classes.input}
                        placeholder="Rechercher un invocateur"
                        inputProps={{'aria-label': 'Rechercher un joueur'}}
                        style={{fontSize: '18px', color: 'black',  width:'15%'}}
                    />
                    <Link to ='/profile-page' style={{textDecoration: "none"}}>
                    <IconButton type="submit" className={classes.iconButton} aria-label="search">
                        <SearchIcon/>
                    </IconButton>
                    </Link>
                    <Divider className={classes.divider} orientation="vertical"/>
                    <div style={{marginBottom: '-5%', marginTop: '3%'}}>

                        <img style={{width: '30%'}} src={Logo}/>
                    </div>
                    <ProductSection/>
                </GridItem>
            </div>
            <Footer/>
        </div>
    );
}
