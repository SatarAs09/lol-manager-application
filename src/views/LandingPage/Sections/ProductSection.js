import React from "react";
// @material-ui/core components
import {makeStyles} from "@material-ui/core/styles";

import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import InfoArea from "components/InfoArea/InfoArea.js";

import styles from "assets/jss/material-kit-react/views/landingPageSections/productStyle.js";
import {Card} from "@material-ui/core";

const useStyles = makeStyles(styles);

export default function ProductSection() {
    const classes = useStyles();
    return (
        <div className={classes.section}>
            <GridContainer justify="center">

                <GridItem xs={12} sm={12} md={8} style={{padding:'10px'}}>
                    <h2 style={{color:'black'}}>Informations</h2>
                </GridItem>
            </GridContainer>
            <div>
                <GridContainer>
                    <GridItem xs={12} sm={12} md={4}>
                        <div style={{padding: '6px', backgroundColor: 'black', webkitBoxShadow: '0px 8px 14px 1px rgba(143,143,143,1)',
                            mozBoxShadow: '0px 8px 14px 1px rgba(143,143,143,1)',
                            boxShadow: '0px 8px 14px 1px rgba(143,143,143,1)'}}>
                            <div style={{border: 'solid 1px gold', padding:'7px' }}>
                                  <h2 style={{color:'gold'}}>Profil</h2>
                                <p style={{color:'white'}}>Vous pouvez créer un profil sur la page de création de compte. Cherchez le bouton « inscription » vers le haut de l'écran. Fournissez toutes les informations nécessaires pour finaliser l'inscription. Vous aurez accès a une page profil que vous retrouverez dans la barre Top menu afin. Cette page est nécéssaire pour supprimer ou modifier vos informations soumises à l'inscritpion.</p>
                            </div>
                        </div>
                    </GridItem>
                    <GridItem xs={12} sm={12} md={4}>
                        <div style={{padding: '6px', backgroundColor: 'black', webkitBoxShadow: '0px 8px 14px 1px rgba(143,143,143,1)',
                            mozBoxShadow: '0px 8px 14px 1px rgba(143,143,143,1)',
                            boxShadow: '0px 8px 14px 1px rgba(143,143,143,1)'}}>
                            <div style={{border: 'solid 1px gold', padding:'7px'}}>
                                <h2 style={{color:'gold'}}>Données</h2>
                                <p style={{color:'white'}}>Le seul moyen de supprimer vos données est de supprimer votre compte. La suppression du compte est irrémédiable. Si vous souhaitez supprimer votre compte définitivement, rendez-vous dans votre compte. Après avoir vérifié que vous êtes bien le propriétaire d'origine du compte, nous pourrons lancer la procédure. Nous attendons généralement 30 jours avant d'effacer le compte, car une fois qu'il est supprimé, il ne peut plus être récupéré. Vous ne pourrez plus vous connecter à votre compte, mais vous pouvez nous envoyer un message jusqu'à 5 jours avant la fin de la période de 30 jours.</p>

                            </div>
                        </div>
                    </GridItem>

                    <GridItem xs={12} sm={12} md={4}>

                        <div style={{padding: '6px', backgroundColor: 'black', webkitBoxShadow: '0px 8px 14px 1px rgba(143,143,143,1)',
                            mozBoxShadow: '0px 8px 14px 1px rgba(143,143,143,1)',
                            boxShadow: '0px 8px 14px 1px rgba(143,143,143,1)'}}>
                            <div style={{border: 'solid 1px gold', padding:'7px'}}>
                                <h2 style={{color:'gold'}}>Création de Partie</h2>
                                <p style={{color:'white'}}>Vous pouvez créer un événement sur la page Événements communautaires. Cherchez le bouton « Créer une partie » vers le haut de l'écran. Fournissez toutes les informations nécessaires pour que nous puissions examiner votre événement. Vous pouvez sauvegarder votre événement à tout moment et le compléter ultérieurement. Riot n'examinera pas votre événement tant que vous ne l'aurez pas envoyé.</p>
                            </div>
                        </div>
                    </GridItem>
                </GridContainer>
            </div>
        </div>
    );
}
