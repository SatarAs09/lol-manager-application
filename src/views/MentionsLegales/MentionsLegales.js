import React from "react";
import classNames from "classnames";
import {makeStyles} from "@material-ui/core/styles";
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Parallax from "components/Parallax/Parallax.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import styles from "assets/jss/material-kit-react/views/components.js";


const useStyles = makeStyles(styles);

export default function Components(props) {
    const classes = useStyles();
    const {...rest} = props;
    return (
        <div>
            <Header
                brand="Material Kit React"
                rightLinks={<HeaderLinks/>}
                fixed
                color="transparent"
                changeColorOnScroll={{
                    height: 400,
                    color: "white"
                }}
                {...rest}
            />
            <Parallax image={require("assets/img/wallhaven-x1kpqz.jpg")}>
                <div className={classes.container}>
                    <GridContainer>
                        <GridItem>

                            <div className={classes.brand} style={{textAlign: 'center'}}>
                                <h1 className={classes.title}>Mentions Légales</h1>
                            </div>

                        </GridItem>
                    </GridContainer>
                </div>
            </Parallax>

            <div className={classNames(classes.main, classes.mainRaised)}
                 style={{backgroundColor: '#F1F1F1', padding: '10px'}}>

                <GridItem md={12} className={classes.textCenter} style={{padding: '2%'}}>




    <h2> <strong>Jargon juridique</strong></h2>
   <p> Dernière mise à jour : Mars 2020 </p>

                    <p>Nous adorons nos joueurs ainsi que leurs idées géniales. Nous soutenons les joueurs qui utilisent notre propriété intellectuelle (« PI ») pour créer des projets fans gratuits au profit de la communauté (les « Projets »). Nous acceptons généralement les Projets qui respectent les règles décrites ci-après, mais pouvons décider à tout moment d'y mettre fin si nous jugeons que ces règles ont été mal interprétées ou qu'il est fait un usage inapproprié de notre PI. </p>

   <h3><strong> 1.Que puis-je développer avec la PI de Riot ?</strong></h3>
    <p>En bref : des contenus gratuits dont la communauté pourra profiter, à quelques exceptions près. </p>

  <p> Par décret formel de la Haute cour de Demacia (notre équipe juridique) : Riot Games (« Riot » ou « Nous ») vous concédons une licence individuelle, non exclusive, non licenciable, non transférable, révocable et limitée d'utiliser, de présenter et de créer des produits dérivés qui reposent sur la PI de Riot, à des fins communautaires strictement non commerciales (sauf indication spécifique ci-dessous), sous réserve respect le plus strict de l'ensemble des règles décrites dans la présente politique (les « Règles ») et des Conditions d'utilisation. Nous nous réservons le droit de refuser à tout moment à quiconque l'usage de notre PI, qu'il y ait un motif quelconque ou non, notamment lorsque nous jugeons, à notre seule discrétion, que l'utilisation que vous faites de notre PI est inappropriée. Si nous vous refusons le droit d'utiliser notre PI, vous devez cesser de développer, publier ou distribuer votre Projet sans délai.</p>

                    <h3><strong>2. Les projets commerciaux sont interdits (à quelques exceptions près...)</strong></h3>
    <p>En bref : nous autorisons les revenus passifs générés grâce à la publicité de certains contenus, abonnements et dons sur les chaînes de streaming ainsi que certains projets commerciaux conformes aux conditions et politiques API utilisant une clé API en cours de validité.</p>

   <p> Vous ne pouvez pas créer de Projets commerciaux sans contrat de licence écrit de notre part, notamment de projets dont une quelconque part de financement est externalisée, impliquant une entreprise ou personne morale ou usant d'un péage informatique ou paywall pour autoriser l'accès aux contenus (p. ex. Patron, YouTube, Premium, etc.). Il y a trois exceptions à cette règle.</p>

    Les trois exceptions.
                    <h3>1ère exception : les revenus publicitaires</h3>
   <p> Chaque joueur est autorisé à promouvoir ses Projets sur des sites web, des flux ou dans des vidéos et de générer des revenus passifs grâce à des publicités adaptées, notamment grâce à des publicités d'avant programme, des coupures publicitaires et l'insertion d'annonces sponsorisées. Pas d'annonces inappropriées : nous seuls jugeons du caractère inapproprié des annonces.</p>

  <h3>  2ème exception : le streaming de Gameplay</h3>
    <p>Chaque joueur est autorisé à solliciter des dons personnels en ligne ou offrir des contenus avec abonnement lors du streaming live de jeux, tant que les joueurs qui n'adhèrent pas peuvent continuer à regarder les jeux simultanément.</p>

   <h3> 3ème exception : conditions d'utilisation et politiques pour développeur API</h3>
   <p> Sont autorisés les projets commerciaux qui 1) respectent nos Conditions APIet nos Politiques API ; et 2) utilisent un clé API Riot en valide octroyée spécifiquement pour le projet en question. Nous nous réservons le droit de distribuer et de révoquer les clés API à notre discrétion. Si nous révoquons votre clé API quel qu'en soit le motif, vous devez clôturer votre projet sans délai.</p>

   <h3><strong> 3. Jeux et apps non admis</strong> </h3>
    <p>Nous interdisons l'usage de notre PI dans les jeux et les apps. Merci de ne transférer ni n'utiliser aucune partie de notre PI dans un jeu ou une app (p. ex. apparence et compétences de personnages, cartes, icônes, objets, etc.). Soyons bien clairs : nous n'autorisons la mise sur Apple Store ou Google Play Store d'aucun jeu qui ne respecte pas nos Conditions d'utilisation et politiques API (décrites précédemment) et pour lequel aucun contrat de licence écrit ni aucune clé API Riot n'ont été octroyés.</p>

    <h3><strong>4. Mon projet doit-il contenir des contenus originaux ?</strong></h3>
    <p>En bref : Oui. Évitez tout simplement de plagier. Soyez créatif !</p>

  <p>  Vous ne pouvez pas utiliser la PI d'un tiers dans votre projet sans son autorisation.
    Ne volez pas les projets d'autrui ; demandez leur autorisation et faites crédit aux créateurs.
    Ne plagiez pas et n'ajoutez pas de brefs commentaires à des contenus existants (p. ex. des parties professionnelles, les vidéos sur demande d'autres joueurs, etc.). Faites bénéficier à la communauté de vos propres contenus originaux. Nous nous réservons le droit d'avoir recours à des services d'identification automatique de nos contenus pour prévenir la cannibalisation des contenus existants.</p>

                    <h3><strong>5. Puis-je utiliser les logos ou marques de Riot ?</strong></h3>
   <p> En bref : non, nous ne voulons pas que les joueurs puissent faire un quelconque rapprochement entre Riot et votre projet.</p>

   <p> Sauf si vous disposez d'un contrat de licence écrit de notre part, vous ne pouvez pas utiliser nos logos ou marques où que ce soit dans votre projet ou sur un site web, dans des contenus publicitaires, des vidéos ou autres types de publications. Vous ne pouvez pas enregistrer de noms de domaine, de comptes de médias sociaux ou autres choses similaires avec Riot Games ou l'une de nos marques, noms commerciaux, noms de personnages, etc. Vos ne pouvez pas utiliser nos marques ou noms commerciaux en rapport avec notre PI comme des mots clés ou tags de recherche.</p>

   <h3><strong> 6. Puis-je partager mon projet avec la communauté ?</strong></h3>
    <p>En bref : oui. Dites juste aux gens qu'il s'agit d'un projet fan, non d'un projet Riot.</p>

   <p> Si vous partagez votre projet, veillez à scrupuleusement inclure l'avis suivant (p. ex. sur le site web de votre projet) :</p>

    <p>[LOL MANAGER] a été créé en vertu de la politique juridique Riot Games intitulée « Jargon juridique » relative à l'utilisation d'actifs de Riot Games.  Riot Games ne soutient ni ne sponsorise ce projet.</p>

   <h3><strong>7. Riot peut-elle utiliser mon projet ?</strong> </h3>
    <p>En bref : oui.</p>

    <p>Nous souhaitons que nos fans créent et partagent des choses sympas les uns pour les autres et leur permettre de les partager. Si nous mettons en vedette un projet fan ou créons quelque chose qui à ce qu'un fan peut avoir développé, nous ne voulons pas être poursuivis en justice, vous nous autorisez donc à mettre en évidence ou partager votre projet avec le monde. Plus spécifiquement, vous acceptez que nous puissions utiliser, reproduire, modifier, distribuer votre projet ou produire des travaux dérivés de ce dernier, quelle qu'en soit la forme, sans redevance, de manière non exclusive, irrévocable, transférable et licenciable et à l'échelle mondiale, quelle qu'en soit la fin et sans devoir vous faire crédit, vous payer quoi que ce soit ni obtenir votre autorisation.</p>

    *           *           *

   <p> En d'autres termes, pour récapituler, il vous suffit de respecter les présentes Règles pour ne pas perturber le Mundo de la bourse et mettre votre projet gratuitement à disposition de la communauté.</p>


                </GridItem>
            </div>
            <Footer/>
        </div>
    );
}
