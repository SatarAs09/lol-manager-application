import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Paper from "@material-ui/core/Paper";
import styles from "assets/jss/material-kit-react/views/loginPage.js";
import image from "assets/img/wallhaven-017vm1.jpg";

const useStyles = makeStyles(styles);

export default function LoginPage(props) {
    const [cardAnimaton, setCardAnimation] = React.useState("cardHidden");
    setTimeout(function() {
        setCardAnimation("");
    }, 700);
    const classes = useStyles();
    const { ...rest } = props;
    return (
        <div>
            <Header
                absolute
                color="transparent"
                brand="Material Kit React"
                rightLinks={<HeaderLinks />}
                {...rest}
            />
            <div
                className={classes.pageHeader}
                style={{
                    backgroundImage: "url(" + image + ")",
                    backgroundSize: "cover",
                    backgroundPosition: "top center"
                }}
            >
                <div className={classes.container}>
                    <GridContainer justify="center">
                        <GridItem  justify="center" xs={12} sm={12} md={12}>

                                <Paper style={{padding:'20px', maxHeight:'570px', overflow:"hidden"}}>
                                    <h3 style={{textAlign:'center', padding:'10px'}}><strong>ZigZag Party</strong></h3>
                                    <div style={{padding:'30px', border: 'double 3px gold'}}>
                                        <h4>5 Participants : </h4>
                                        <p style={{ padding:'10px', backgroundColor:'#F1F1F1'}}>Yuri</p>
                                        <p style={{ padding:'10px', backgroundColor:'#F2F2F2'}}>Zank</p>
                                        <p style={{ padding:'10px', backgroundColor:'#F3F3F3'}}>Symbolic</p>
                                        <p style={{ padding:'10px', backgroundColor:'#F4F4F4'}}>Sataras</p>
                                        <p style={{ padding:'10px', backgroundColor:'#F5F5F5'}}>Chefkrezus</p>
                                        <p>Date de la partie : <strong>10/11/2020</strong></p>
                                    </div>
                                </Paper>
                        </GridItem>
                    </GridContainer>
                </div>
                <Footer whiteFont />
            </div>
        </div>
    );
}
