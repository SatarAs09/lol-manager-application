import React, { PureComponent } from 'react';
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

const data = [
    {
        name: 'Inscription', uv: 4000, pv: 2400, amt: 2400,
    },
    {
        name: 'Connexion', uv: 3000, pv: 1398, amt: 2210,
    },
    {
        name: 'Parties créées', uv: 2000, pv: 1789, amt: 2290,
    },
    {
        name: 'Tournois crées', uv: 2000, pv: 200, amt: 2290,
    },

];

export default class Example extends PureComponent {
    static jsfiddleUrl = 'https://jsfiddle.net/alidingling/q4eonc12/';

    render() {
        return (
            <div>
                <h4>Stats</h4>
            <BarChart

                width={400}
                height={200}
                data={data}
                margin={{
                    top: 5, right: 30, left: 20, bottom: 5,
                }}
                barSize={20}
            >
                <XAxis dataKey="name" scale="point" ackground={{ fill: 'gold', }} padding={{ left: 10, right: 10 }} />
                <YAxis />
                <Tooltip />
                <Legend />
                <CartesianGrid strokeDasharray="3 3"  />
                <Bar dataKey="pv" fill="gold" background={{ fill: '#eee', Color: 'gold' }} />
            </BarChart>
            </div>
        );
    }
}
