import React, { PureComponent } from 'react';
import Paper from '@material-ui/core/Paper'
import {
    AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip,
} from 'recharts';

const data = [
    {
        name: 'Accueil', uv: 4000, pv: 2400, amt: 2400,
    },
    {
        name: 'Inscription', uv: 3000, pv: 1398, amt: 2210,
    },
    {
        name: 'Profil', uv: 2000, pv: 9800, amt: 2290,
    },
    {
        name: 'Création partie', uv: 2780, pv: 3908, amt: 2000,
    },

];

export default class Example extends PureComponent {
    static jsfiddleUrl = 'https://jsfiddle.net/alidingling/Lrffmzfc/';

    render() {
        return (
            <div>
                    <h4>Traffic</h4>
                <AreaChart
                width={400}
                height={300}
                data={data}
                margin={{
                    top: 10, right: 30, left: 0, bottom: 0,
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Area type="monotone"  dataKey="uv" stroke="#8884d8" fill="gold" />
            </AreaChart>
            </div>

        );
    }
}

