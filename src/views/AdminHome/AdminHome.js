import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import image from "assets/img/wallhaven-017vm1.jpg";
import ListItem from '@material-ui/core/ListItem';
import PeopleIcon from '@material-ui/icons/People';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import {Link} from "react-router-dom";
import GamesIcon from '@material-ui/icons/Games';
import GradientIcon from "@material-ui/icons/Gradient";
import HomeIcon from '@material-ui/icons/Home';
import profile from "assets/img/logoprojet.png";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from '@material-ui/core/Card';
import KeyboardReturnIcon from '@material-ui/icons/KeyboardReturn';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import background1 from 'assets/img/wallhaven-3kov8v.jpg';
import background2 from 'assets/img/wallhaven-k9eqdm.jpg';
import background3 from 'assets/img/wallhaven-3kovev.jpg';
import Stats from './Stats';
import Stats2 from './Stats2';
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        icon: {
            fill:'gold',

        }
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,

        },
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
            background: 'linear-gradient(-249.36882712449835deg, rgba(0, 0, 0,1) 2.392578125%,rgba(1, 96, 186,1) 91.58528645833333%)',
            color: 'white'
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
        backgroundImage: "url(" + image + ")",
        color: 'white'

    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),


    },
    table: {
        minWidth: 650,
    },
    Card: {
      backgroundImage: `url(${background1})`,
        backgroundSize: 'cover',
        background: 'center',
        height: 200,
    },
    Card2: {
        backgroundImage: `url(${background2})`,
        backgroundSize: 'cover',
        background: 'center',
        height: 200,
    },
    Card3: {
        backgroundImage: `url(${background3})`,
        backgroundSize: 'cover',
        background: 'center',
        height: 200,
    },


}));

function ResponsiveDrawer(props) {
    const {container} = props;
    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <div>
            <Link to ='/admin' style={{textDecoration: "none"}}>
             <img style={{width:'120px', marginLeft:'25%', marginTop: '3%'}} src={profile} alt="..."  />
            </Link>
            <div className={classes.toolbar}/>

            <List>
                <Link to ='/admin' style={{textDecoration: "none"}}>
                    <ListItem button>
                        <ListItemIcon style={{color:"gold", fill:'gold'}}>
                            <HomeIcon className={classes.icon} />
                        </ListItemIcon>
                        <ListItemText primary="Accueil" />
                    </ListItem>
                </Link>
                <Link to ='/admin-page' style={{textDecoration: "none"}}>
                    <ListItem button>
                        <ListItemIcon style={{color:"gold", fill:'gold'}}>
                            <PeopleIcon className={classes.icon} />
                        </ListItemIcon>
                        <ListItemText primary="Utilisateurs" />
                    </ListItem>
                </Link>
                <Link to ='/admin-partie-page' style={{textDecoration: "none"}}>
                    <ListItem button>
                        <ListItemIcon style={{color:"gold", fill:'gold'}}>
                            <GamesIcon className={classes.icon} />
                        </ListItemIcon>
                        <ListItemText primary="Parties" />
                    </ListItem>
                </Link>
                <Link to ='/admin-tournois' style={{textDecoration: "none"}}>
                    <ListItem button>
                        <ListItemIcon style={{color:"gold", fill:'gold'}}>
                            <GradientIcon className={classes.icon} />
                        </ListItemIcon>
                        <ListItemText primary="Tournois" />
                    </ListItem>
                </Link>
              <hr style={{width: '90px'}}/>
                <Link to ='/' style={{textDecoration: "none"}}>
                    <ListItem button>
                        <ListItemIcon style={{color:"gold", fill:'gold'}}>
                            <KeyboardReturnIcon className={classes.icon} />
                        </ListItemIcon>
                        <ListItemText primary="Retour sur le site" />
                    </ListItem>
                </Link>

            </List>


        </div>
    );

    return (
        <div className={classes.root}>
            <CssBaseline/>
            <AppBar position="fixed" justify="center" className={classes.appBar}>
                <Toolbar justify="center">
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon/>
                    </IconButton>
                    <Typography align="center" variant="h5" noWrap>
                        Accueil
                    </Typography>
                </Toolbar>
            </AppBar>
            <nav className={classes.drawer} aria-label="mailbox folders">
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}

                    <Hidden smUp implementation="css">

                    <Drawer
                        container={container}
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {drawer}
                    </Drawer>

                </Hidden>
                <Hidden xsDown implementation="css">
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,

                        }}
                        variant="permanent"
                        open
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
            <main className={classes.content} style={{backgroundColor: '#h2h2h2'}}>
                <div className={classes.toolbar}/>

                <GridContainer align="center"  >
                    <GridItem  xs={12} sm={12} md={4}>
                       <Link to='/admin-page'>
                        <Card className={classes.Card2} variant="outlined">
                            <CardActionArea style={{height: 200}}>

                                <CardContent>


                                </CardContent>
                                <h1 style={{color:'white'}}>Utilisateurs</h1>
                            </CardActionArea>

                        </Card>
                       </Link>
                    </GridItem>
                    <GridItem  xs={12} sm={12} md={4}>
                        <Link to='/admin-partie-page'>
                            <Card className={classes.Card} variant="outlined" >
                                <CardActionArea style={{height: 200}}>

                                    <CardContent>
                                    </CardContent>

                                    <h1 style={{color:'gold'}}>Parties</h1>
                                </CardActionArea>

                            </Card>
                        </Link>
                    </GridItem>
                    <GridItem  xs={12} sm={12} md={4}>
                        <Link to='/admin-tournois'>
                            <Card className={classes.Card3} variant="outlined">
                                <CardActionArea style={{height: 200}}>

                                    <CardContent>
                                    </CardContent>

                                    <h1 style={{color:'white'}}>Tournois</h1>
                                </CardActionArea>

                            </Card>
                        </Link>

                    </GridItem>
                </GridContainer>
                <GridContainer style={{marginTop:'5%'}} align="center"  >
                    <GridItem  xs={12} sm={12} md={4}>
                        <Stats/>

                    </GridItem>
                    <GridItem  xs={12} sm={12} md={4}>
                        <Stats2 />
                    </GridItem>
                </GridContainer>

            </main>
        </div>
    );
}

ResponsiveDrawer.propTypes = {
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    container: PropTypes.any,
};

export default ResponsiveDrawer;
